Install CMake (latest)
Install Visual Studio 2017 community
install Qt 5.12 (including QScript?)
install jom http://wiki.qt.io/Jom
install Git (+patch + perl): http://git-scm.com/download/win
install python 3.x (https://www.python.org/downloads/ or through VisualStudio) + add to path
install nsis: http://sourceforge.net/projects/nsis/files/latest/download?source=directory
        and install the NSIS Inetc plugin (Ascii version dll). 
        Copy Inetc/Plugins/x86-ansi/INetC.dll => NSIS/Plugins/x86-ansi
install Flex & Bison https://sourceforge.net/projects/winflexbison/ (extract the whole zip to C:\Program Files (x86)\win_flex_bison\)

Optionally install
An nsis editor to change the install script: http://hmne.sourceforge.net/



# visual studio envs 32 bit
"C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\Tools\VsDevCmd.bat"
# or
# visual studio envs 64 bit
"C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" amd64


# Qt
set PATH=%PATH%;C:\Qt\5.6\msvc2015\bin

# Git and patch.exe
set PATH=%PATH%;"C:\Program Files\Git\bin"

# patch.exe and perl.exe
set PATH=%PATH%;"C:\Program Files\Git\usr\bin"

# NSIS
set PATH=%PATH%;"C:\Program Files (x86)\NSIS"

# Run Cmake NOTE the -DPERL_EXECUTABLE=... at the end
# CMake does not seem to find perl from the Git binary directory without this help.
cmake -G "NMake Makefiles JOM" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=..\install\kate ..\ -DPERL_EXECUTABLE="C:\\Program Files\\Git\\usr\\bin\\perl.exe" -DFLEX_EXECUTABLE="C:\\Program Files (x86)\\win_flex_bison\\win_flex.exe" -DBISON_EXECUTABLE="C:\\Program Files (x86)\\win_flex_bison\\win_bison.exe"

# build
cmake --build . --config Release


#Basically Copy Paste:
mkdir build
cd build


32Bit Qt 5.6 MSVC 2015:
"C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\Tools\VsDevCmd.bat"
set PATH=C:\Qt\5.9.1\msvc2015\bin;%PATH%
set PATH=%PATH%;"C:\Program Files\Git\bin"
set PATH=%PATH%;"C:\Program Files\Git\usr\bin"
set PATH=%PATH%;"C:\Program Files (x86)\NSIS"
cmake -G "NMake Makefiles JOM" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=..\i_tar_32 ..\ -DPERL_EXECUTABLE="C:\\Program Files\\Git\\usr\\bin\\perl.exe" -DFLEX_EXECUTABLE="C:\\Program Files (x86)\\win_flex_bison\\win_flex.exe" -DBISON_EXECUTABLE="C:\\Program Files (x86)\\win_flex_bison\\win_bison.exe"
cmake --build . --config Release

# OR

64Bit Qt 5.6 MSVC 2015:
"C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" amd64
set PATH=C:\Qt\5.9.1\msvc2015_64\bin;%PATH%
set PATH=%PATH%;"C:\Program Files\Git\bin"
set PATH=%PATH%;"C:\Program Files\Git\usr\bin"
set PATH=%PATH%;"C:\Program Files (x86)\NSIS"
cmake -G "NMake Makefiles JOM" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=..\i_tar_64 ..\ -DPERL_EXECUTABLE="C:\\Program Files\\Git\\usr\\bin\\perl.exe" -DFLEX_EXECUTABLE="C:\\Program Files (x86)\\win_flex_bison\\win_flex.exe" -DBISON_EXECUTABLE="C:\\Program Files (x86)\\win_flex_bison\\win_bison.exe"
cmake --build . --config Release

# OR

64Bit Qt 5.6 MSVC 2015 KF5/Kate Master:
"C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" amd64
set PATH=C:\Qt\5.9.1\msvc2015_64\bin;%PATH%
set PATH=%PATH%;"C:\Program Files\Git\bin"
set PATH=%PATH%;"C:\Program Files\Git\usr\bin"
set PATH=%PATH%;"C:\Program Files (x86)\NSIS"
cmake -G "NMake Makefiles JOM" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=..\i_tar_64 ..\ -DPERL_EXECUTABLE="C:\\Program Files\\Git\\usr\\bin\\perl.exe" -DFLEX_EXECUTABLE="C:\\Program Files (x86)\\win_flex_bison\\win_flex.exe" -DBISON_EXECUTABLE="C:\\Program Files (x86)\\win_flex_bison\\win_bison.exe" -DKF5_VERSION=master -DKATE_VERSION=master
cmake --build . --config Release




64Bit Qt 5.12.46 VS 2017
"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars64.bat"
set PATH=c:\Qt\5.12.6\msvc2017_64\bin;%PATH%
set PATH=%PATH%;"C:\Program Files (x86)\NSIS"

cmake -G "NMake Makefiles JOM" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=..\i_tar_64 ..\ -DPERL_EXECUTABLE="C:\\Program Files\\Git\\usr\\bin\\perl.exe" -DFLEX_EXECUTABLE="C:\\Program Files (x86)\\win_flex_bison\\win_flex.exe" -DBISON_EXECUTABLE="C:\\Program Files (x86)\\win_flex_bison\\win_bison.exe"

cmake -G "Ninja" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=..\i_tar_64 ..\ -DPERL_EXECUTABLE="C:\\Program Files\\Git\\usr\\bin\\perl.exe" -DFLEX_EXECUTABLE="C:\\Program Files (x86)\\win_flex_bison\\win_flex.exe" -DBISON_EXECUTABLE="C:\\Program Files (x86)\\win_flex_bison\\win_bison.exe"
